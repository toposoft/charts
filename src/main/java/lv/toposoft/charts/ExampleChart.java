package lv.toposoft.charts;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.LegendItem;
import org.jfree.chart.LegendItemCollection;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.SubCategoryAxis;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.labels.CategoryItemLabelGenerator;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.CombinedRangeCategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.GroupedStackedBarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.KeyToGroupMap;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RectangleEdge;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.awt.geom.RectangularShape;
import java.text.NumberFormat;
import java.util.Locale;

@Service
public class ExampleChart extends ApplicationFrame {
    private Paint grey = new GradientPaint(0.0f, 0.0f, new Color(166, 166, 166),
            0.0f, 0.0f, new Color(166, 166, 166));
    private Color orange = new Color(247, 146, 30);
    private Paint lightorange = new Color(250, 190, 120);
    private Paint blue = new Color(191, 231, 255);
    private Paint darkblue = new Color(37, 137, 199);
    private Stroke dashed =  new BasicStroke(3.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, new float[] {10.0f, 5.0f}, 0.0f);
    private Font font = new Font("OpenSans", Font.ITALIC, 14);
    private Font itemfont = new Font("OpenSans", Font.BOLD, 14);
    private static final Color TRANSPARENT = new Color(255, 255, 255, 0);

    public ExampleChart(){
        super("Chart");
        init();
    }

    public ExampleChart(String title) {
        super(title);
        init();
    }

    private void init(){
        final CategoryDataset dataset = createDataset1();
        final JFreeChart chart = createChart(dataset);
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(800, 500));
        setContentPane(chartPanel);
    }

    private CategoryDataset createDataset() {
        DefaultCategoryDataset result = new DefaultCategoryDataset();

        result.addValue(12850, "UtilityBillAfter6Growth NS", "6% Annual Electric Bill Growth");
        return result;
    }

    private CategoryDataset createDataset1() {
        DefaultCategoryDataset result = new DefaultCategoryDataset();

        result.addValue(2150, "UtilityBillAfter6Growth WS", "6% Annual Electric Bill Growth");
        result.addValue(7000, "LoanOrangeTotalHeight6Growth WS", "6% Annual Electric Bill Growth");
        result.addValue(750, "LightOrangeTotalHeigh6Growth WS", "6% Annual Electric Bill Growth");
        result.addValue(2450, "LoanLightBlueTotalHeight6Growth", "6% Annual Electric Bill Growth");
        return result;
    }

    private CategoryDataset createDataset2() {
        DefaultCategoryDataset result = new DefaultCategoryDataset();

        result.addValue(1100, "UtilityBillAfter0Growth", "0% Annual Electric Bill Growth");
        result.addValue(7000, "LoanOrangeTotalHeight0Growth", "0% Annual Electric Bill Growth");
        result.addValue(750, "LightOrangeTotalHeigh0Growth", "0% Annual Electric Bill Growth");
        result.addValue(1100, "LoanLightBlueTotalHeight0Growth", "0% Annual Electric Bill Growth");

        return result;
    }

    private JFreeChart createChart(final CategoryDataset dataset) {

        GroupedStackedBarRenderer.setDefaultShadowsVisible(false);
        BarRenderer.setDefaultShadowsVisible(false);

        GroupedStackedBarRenderer.setDefaultBarPainter(new StandardBarPainter() {
            public void paintBarShadow(Graphics2D g2, BarRenderer renderer, int row,
                                       int column, RectangularShape bar, RectangleEdge base,
                                       boolean pegShadow) {}
        });
        GroupedStackedBarRenderer renderer6ns = new GroupedStackedBarRenderer();
        renderer6ns.setMaximumBarWidth(0.6);

        GroupedStackedBarRenderer renderer6 = new GroupedStackedBarRenderer();
        renderer6.setMaximumBarWidth(0.6);

        GroupedStackedBarRenderer renderer0 = new GroupedStackedBarRenderer();
        renderer0.setMaximumBarWidth(0.6);

        renderer6ns.setBaseToolTipGenerator(
                new StandardCategoryToolTipGenerator());
        renderer6.setBaseToolTipGenerator(
                new StandardCategoryToolTipGenerator());
        renderer0.setBaseToolTipGenerator(
                new StandardCategoryToolTipGenerator());

        renderer0.setBaseItemLabelGenerator(new CategoryItemLabelGenerator() {

            @Override
            public String generateRowLabel(CategoryDataset dataset, int row) {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public String generateLabel(CategoryDataset dataset, int row, int column) {
                double amount = dataset.getValue(row, column).doubleValue();
                Locale locale = new Locale("en", "US");
                NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
                return row == 3 ? currencyFormatter.format(amount) : "";
            }

            @Override
            public String generateColumnLabel(CategoryDataset dataset, int column) {
                // TODO Auto-generated method stub
                return null;
            }
        });
        renderer0.setBaseItemLabelsVisible(true);
        renderer0.setBaseItemLabelFont(itemfont);
        renderer0.setBaseItemLabelPaint(darkblue);

        renderer6.setBaseItemLabelGenerator(new CategoryItemLabelGenerator() {

            @Override
            public String generateRowLabel(CategoryDataset dataset, int row) {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public String generateLabel(CategoryDataset dataset, int row, int column) {
                double amount = dataset.getValue(row, column).doubleValue();
                Locale locale = new Locale("en", "US");
                NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
                return row == 3 ? currencyFormatter.format(amount) : "";
            }

            @Override
            public String generateColumnLabel(CategoryDataset dataset, int column) {
                // TODO Auto-generated method stub
                return null;
            }
        });
        renderer6.setBaseItemLabelsVisible(true);
        renderer6.setBaseItemLabelFont(itemfont);
        renderer6.setBaseItemLabelPaint(darkblue);

        CategoryDataset dataset0 = createDataset();
        CategoryDataset dataset1 = dataset;
        CategoryDataset dataset2 = createDataset2();

        // Subcategories
        SubCategoryAxis domainAxis0 = new SubCategoryAxis(""); // 0% Annual Electric Bill Growth
        SubCategoryAxis domainAxis6 = new SubCategoryAxis(""); // 6% Annual Electric Bill Growth
        SubCategoryAxis domainAxis6ns = new SubCategoryAxis("");

        CategoryPlot subplot0 = new CategoryPlot(dataset0, domainAxis6ns, null,
                renderer6ns);
        CategoryPlot subplot1 = new CategoryPlot(dataset1, domainAxis6, null,
                renderer6);
        CategoryPlot subplot2 = new CategoryPlot(dataset2, domainAxis0, null,
                renderer0);

        subplot0.setDomainGridlinesVisible(false);
        subplot1.setDomainGridlinesVisible(false);
        subplot2.setDomainGridlinesVisible(false);

        subplot0.setOutlinePaint(TRANSPARENT);
        subplot1.setOutlinePaint(TRANSPARENT);
        subplot2.setOutlinePaint(TRANSPARENT);

        subplot0.setDomainAxisLocation(AxisLocation.TOP_OR_LEFT);
        subplot1.setDomainAxisLocation(AxisLocation.TOP_OR_LEFT);
        subplot2.setDomainAxisLocation(AxisLocation.TOP_OR_LEFT);


        KeyToGroupMap map = new KeyToGroupMap("G1");
        map.mapKeyToGroup("UtilityBillAfter6Growth NS", "G1");
        renderer6ns.setSeriesToGroupMap(map);

        KeyToGroupMap map1 = new KeyToGroupMap("G2");
        map1.mapKeyToGroup("UtilityBillAfter6Growth WS", "G2");
        map1.mapKeyToGroup("LoanOrangeTotalHeight6Growth WS", "G2");
        map1.mapKeyToGroup("LightOrangeTotalHeigh6Growth WS", "G2");
        map1.mapKeyToGroup("LoanLightBlueTotalHeight6Growth", "G2");
        renderer6.setSeriesToGroupMap(map1);

        KeyToGroupMap map2 = new KeyToGroupMap("G3");
        map2.mapKeyToGroup("UtilityBillAfter0Growth", "G3");
        map2.mapKeyToGroup("LoanOrangeTotalHeight0Growth", "G3");
        map2.mapKeyToGroup("LightOrangeTotalHeigh0Growth", "G3");
        map2.mapKeyToGroup("LoanLightBlueTotalHeight0Growth", "G3");
        renderer0.setSeriesToGroupMap(map2);


        // Colors
        renderer6ns.setItemMargin(0.05);
        renderer6ns.setSeriesPaint(0, grey);
        renderer6ns.setDrawBarOutline(true);

        renderer6.setItemMargin(0.05);
        renderer6.setSeriesPaint(0, grey);
        renderer6.setSeriesPaint(1, orange);
        renderer6.setSeriesPaint(2, lightorange);
        renderer6.setSeriesPaint(3, blue);
        renderer6.setSeriesOutlineStroke(3, dashed);
        renderer6.setSeriesOutlinePaint(3, darkblue);
        renderer6.setSeriesFillPaint(3, blue);
        renderer6.setDrawBarOutline(true);

        renderer0.setItemMargin(0.05);
        renderer0.setSeriesPaint(0, grey);
        renderer0.setSeriesPaint(1, orange);
        renderer0.setSeriesPaint(2, lightorange);
        renderer0.setSeriesPaint(3, blue);
        renderer0.setSeriesOutlineStroke(3, dashed);
        renderer0.setSeriesOutlinePaint(3, darkblue);
        renderer0.setDrawBarOutline(true);

        domainAxis6ns.setCategoryLabelPositionOffset(15);
        domainAxis6.setCategoryLabelPositionOffset(15);
        domainAxis0.setCategoryLabelPositionOffset(15);

        domainAxis6ns.setSubLabelFont(new Font(Font.SANS_SERIF, Font.BOLD, 12));
        domainAxis6.setSubLabelFont(new Font(Font.SANS_SERIF, Font.BOLD, 12));
        domainAxis0.setSubLabelFont(new Font(Font.SANS_SERIF, Font.BOLD, 12));

        domainAxis6ns.addSubCategory("No solar");
        domainAxis6.addSubCategory("With SunPower");
        domainAxis0.addSubCategory("With SunPower");

        domainAxis6ns.setTickLabelsVisible(false);

        domainAxis6ns.setTickLabelFont(font);
        domainAxis6.setTickLabelFont(font);
        domainAxis0.setTickLabelFont(font);

        domainAxis6ns.setAxisLineVisible(false);
        domainAxis6.setAxisLineVisible(false);
        domainAxis0.setAxisLineVisible(false);

        domainAxis6ns.setMaximumCategoryLabelLines(2);
        domainAxis6.setMaximumCategoryLabelLines(2);
        domainAxis0.setMaximumCategoryLabelLines(2);

        domainAxis6.setLabel("\n \n");
        domainAxis0.setLabel("\n \n");

        final NumberAxis rangeAxis = new NumberAxis("Estimated Payments and Savings Over 25 Years");
        NumberFormat currencyFormatter =  NumberFormat.getCurrencyInstance(Locale.US);
        currencyFormatter.setMaximumFractionDigits(0);
        rangeAxis.setNumberFormatOverride(currencyFormatter);

        CombinedRangeCategoryPlot plot = new CombinedRangeCategoryPlot(rangeAxis);
        plot.setRangePannable(true);

        plot.add(subplot0, 1);
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        plot.setGap(0);
        plot.setOutlineVisible(true);

        JFreeChart chart = new JFreeChart("",
                new Font("SansSerif", Font.BOLD, 12), plot, true);

        chart.getLegend().setFrame(BlockBorder.NONE);
        chart.getLegend().setPosition(RectangleEdge.BOTTOM);
        chart.getPlot().setBackgroundPaint(Color.white);

        // all bars
        plot.getRangeAxis().setMinorTickMarkOutsideLength(22F);
        plot.setFixedLegendItems(
                createLegendItems(
                        new String[]{
                                "Electric Bill","Solar Lease Payments","30% Paydown at Mo.18","Net Savings"
                        },
                        new Paint[]{
                                grey, orange, lightorange, blue
                        }));
        plot.setRangeGridlinePaint(Color.black);

        return chart;
    }

    private LegendItemCollection createLegendItems(String[] labels, Paint[] colors) {
        LegendItemCollection result = new LegendItemCollection();
        for(int i=0; i < labels.length; i++){
            LegendItem item = new LegendItem(labels[i],labels[i],labels[i],labels[i], new Rectangle(20, 20), colors[i]);
            result.add(item);
        }
        return result;
    }

}