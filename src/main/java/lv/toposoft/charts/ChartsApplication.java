package lv.toposoft.charts;

import org.jfree.ui.RefineryUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class ChartsApplication implements CommandLineRunner {
	@Autowired
	private ExampleChart chart;
	@Autowired
	private LoanChart groupChart;
	@Autowired
	private CashChart cashChart;
	@Autowired
	private LeaseChart leaseChart;


	public static void main(String[] args) {
		new SpringApplicationBuilder(ChartsApplication.class)
				.headless(false)
				.web(false)
				.run(args);
	}

	@Override
	public void run(String... args) throws Exception {
/*
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				chart.pack();
				RefineryUtilities.centerFrameOnScreen(chart);
				chart.setVisible(true);
			}
		});
*/

		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				groupChart.pack();
				RefineryUtilities.centerFrameOnScreen(groupChart);
				groupChart.setVisible(true);
			}
		});

		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				cashChart.pack();
				RefineryUtilities.centerFrameOnScreen(cashChart);
				cashChart.setVisible(true);
			}
		});

		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				leaseChart.pack();
				RefineryUtilities.centerFrameOnScreen(leaseChart);
				leaseChart.setVisible(true);
			}
		});
	}

}
