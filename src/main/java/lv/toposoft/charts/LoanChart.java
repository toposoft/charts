package lv.toposoft.charts;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.LegendItem;
import org.jfree.chart.LegendItemCollection;
import org.jfree.chart.axis.*;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.labels.CategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.CombinedRangeCategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.GroupedStackedBarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.KeyToGroupMap;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RectangleEdge;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.awt.geom.RectangularShape;
import java.text.NumberFormat;
import java.util.Locale;

@Service
public class LoanChart extends ApplicationFrame {
    private Paint grey = new GradientPaint(0.0f, 0.0f, new Color(166, 166, 166),
            0.0f, 0.0f, new Color(166, 166, 166));
    private Color orange = new Color(247, 146, 30);
    private Paint lightorange = new Color(250, 190, 120);
    private Paint blue = new Color(191, 231, 255);
    private Paint darkblue = new Color(37, 137, 199);
    private Stroke dashed =  new BasicStroke(3.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, new float[] {10.0f, 5.0f}, 0.0f);
    private Font font = new Font("OpenSans", Font.ITALIC, 14);
    private Font itemfont = new Font("OpenSans", Font.BOLD, 16);
    private static final Color TRANSPARENT = new Color(255, 255, 255, 0);

    public LoanChart(){
        super("Loan");
        init();
    }

    public LoanChart(String title) {
        super(title);
        init();
    }

    private void init(){
        final CategoryDataset dataset = createDataset();
        final JFreeChart chart = createChart(dataset);
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(800, 500));
        setContentPane(chartPanel);
    }

    private CategoryDataset createDataset() {
        DefaultCategoryDataset result = new DefaultCategoryDataset();

        result.addValue(408397.4936915188, "UtilityBillBefore4Growth", "4% Annual Electric Bill Growth");
        result.addValue(349715.46772798663, "UtilityBillAfter4Growth", "4% Annual Electric Bill Growth");
        result.addValue(17000, "totalLoanPayments", "4% Annual Electric Bill Growth");
        result.addValue(7750, "ITCRateMo18", "4% Annual Electric Bill Growth");
        result.addValue(-36668, "savings25Years", "4% Annual Electric Bill Growth");

        return result;
    }

    private CategoryDataset createDataset0() {
        DefaultCategoryDataset result = new DefaultCategoryDataset();

        result.addValue(155821.5, "UtilityBillAfter0Growth", "0% Annual Electric Bill Growth");
        result.addValue(17000, "totalLoanPayments", "0% Annual Electric Bill Growth");
        result.addValue(7750, "ITCRateMo18", "0% Annual Electric Bill Growth");
        result.addValue(-36668, "savings25Years", "0% Annual Electric Bill Growth");

        return result;
    }

    private JFreeChart createChart(final CategoryDataset dataset) {

        KeyToGroupMap map = new KeyToGroupMap("G1");
        map.mapKeyToGroup("UtilityBillBefore4Growth", "G1");

        map.mapKeyToGroup("UtilityBillAfter4Growth", "G2");
        map.mapKeyToGroup("totalLoanPayments", "G2");
        map.mapKeyToGroup("ITCRateMo18", "G2");
        map.mapKeyToGroup("savings25Years", "G2");

        KeyToGroupMap map0 = new KeyToGroupMap("G3");
        map0.mapKeyToGroup("UtilityBillAfter0Growth", "G3");
        map0.mapKeyToGroup("totalLoanPayments", "G3");
        map0.mapKeyToGroup("ITCRateMo18", "G3");
        map0.mapKeyToGroup("savings25Years", "G3");

        GroupedStackedBarRenderer.setDefaultShadowsVisible(false);
        BarRenderer.setDefaultShadowsVisible(false);

        GroupedStackedBarRenderer.setDefaultBarPainter(new StandardBarPainter() {
            public void paintBarShadow(Graphics2D g2, BarRenderer renderer, int row,
                                       int column, RectangularShape bar, RectangleEdge base,
                                       boolean pegShadow) {}
        });

        GroupedStackedBarRenderer renderer4 = new GroupedStackedBarRenderer();
        renderer4.setMaximumBarWidth(0.3);

        GroupedStackedBarRenderer renderer0 = new GroupedStackedBarRenderer();
        renderer0.setMaximumBarWidth(0.6);

        renderer0.setBaseItemLabelGenerator(new CategoryItemLabelGenerator() {

            @Override
            public String generateRowLabel(CategoryDataset dataset, int row) {
                return "";
            }

            @Override
            public String generateLabel(CategoryDataset dataset, int row, int column) {
                double amount = dataset.getValue(row, column).doubleValue();
                Locale locale = new Locale("en", "US");
                NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
                return row == 3 ? currencyFormatter.format(amount) : "";
            }

            @Override
            public String generateColumnLabel(CategoryDataset dataset, int column) {
                return "";
            }
        });
        renderer0.setBaseItemLabelsVisible(true);
        renderer0.setBaseItemLabelFont(itemfont);
        renderer0.setBaseItemLabelPaint(darkblue);

        renderer4.setBaseItemLabelGenerator(new CategoryItemLabelGenerator() {

            @Override
            public String generateRowLabel(CategoryDataset dataset, int row) {
                return null;
            }

            @Override
            public String generateLabel(CategoryDataset dataset, int row, int column) {
                double amount = dataset.getValue(row, column).doubleValue();
                Locale locale = new Locale("en", "US");
                NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
                return row == 4 ? currencyFormatter.format(amount) : "";
            }

            @Override
            public String generateColumnLabel(CategoryDataset dataset, int column) {
                return null;
            }
        });
        renderer4.setBaseItemLabelsVisible(true);
        renderer4.setBaseItemLabelFont(itemfont);
        renderer4.setBaseItemLabelPaint(darkblue);

        // Subcategories
        SubCategoryAxis domainAxis0 = new SubCategoryAxis("0% Annual Electric Bill Growth"); // 0% Annual Electric Bill Growth
        SubCategoryAxis domainAxis4 = new SubCategoryAxis("4% Annual Electric Bill Growth");

        CategoryPlot subplot4 = new CategoryPlot(dataset, domainAxis4, null, renderer4);

        CategoryPlot subplot0 = new CategoryPlot(createDataset0(), domainAxis0, null, renderer0);

        subplot4.setDomainGridlinesVisible(false);
        subplot0.setDomainGridlinesVisible(false);
        subplot0.setRangeGridlinesVisible(false);
        subplot4.setRangeGridlinesVisible(false);
        subplot0.setBackgroundPaint(Color.WHITE);
        subplot4.setBackgroundPaint(Color.WHITE);
        subplot0.setOutlineVisible(false);
        subplot4.setOutlineVisible(false);

        subplot4.setOutlinePaint(TRANSPARENT);
        subplot0.setOutlinePaint(TRANSPARENT);

        subplot4.setDomainAxisLocation(AxisLocation.TOP_OR_LEFT);
        subplot0.setDomainAxisLocation(AxisLocation.TOP_OR_LEFT);

        renderer4.setSeriesToGroupMap(map);
        renderer0.setSeriesToGroupMap(map0);


        // Colors
        renderer4.setItemMargin(0.05);
        renderer4.setSeriesPaint(0, grey);

        renderer4.setSeriesPaint(1, grey);
        renderer4.setSeriesPaint(2, orange);
        renderer4.setSeriesPaint(3, lightorange);
        renderer4.setSeriesPaint(4, blue);
        renderer4.setSeriesOutlineStroke(4, dashed);
        renderer4.setSeriesOutlinePaint(4, darkblue);
        renderer4.setSeriesFillPaint(4, blue);

        renderer0.setItemMargin(0.05);
        renderer0.setSeriesPaint(0, grey);
        renderer0.setSeriesPaint(1, orange);
        renderer0.setSeriesPaint(2, lightorange);
        renderer0.setSeriesPaint(3, blue);
        renderer0.setSeriesOutlineStroke(3, dashed);
        renderer0.setSeriesOutlinePaint(3, darkblue);
        renderer0.setSeriesFillPaint(3, blue);

        renderer0.setDrawBarOutline(false);
        renderer4.setDrawBarOutline(false);

        domainAxis4.setCategoryLabelPositionOffset(15);
        domainAxis0.setCategoryLabelPositionOffset(15);

        domainAxis4.setSubLabelFont(new Font("OpenSans", Font.PLAIN, 12));
        domainAxis0.setSubLabelFont(new Font("OpenSans", Font.PLAIN, 12));

        domainAxis4.addSubCategory("No solar");
        domainAxis4.addSubCategory("With SunPower");
        domainAxis0.addSubCategory("With SunPower");

        domainAxis4.setTickLabelsVisible(true);

        domainAxis4.setTickLabelFont(font);
        domainAxis0.setTickLabelFont(font);

        domainAxis4.setAxisLineVisible(false);
        domainAxis0.setAxisLineVisible(false);

        domainAxis4.setMaximumCategoryLabelLines(2);
        domainAxis0.setMaximumCategoryLabelLines(2);

        domainAxis0.setLabel("\n \n");
        domainAxis4.setLabel("\n \n");

        final NumberAxis rangeAxis = new NumberAxis("Estimated Payments and Savings Over 25 Years");
        NumberFormat currencyFormatter =  NumberFormat.getCurrencyInstance(Locale.US);
        currencyFormatter.setMaximumFractionDigits(0);
        rangeAxis.setNumberFormatOverride(currencyFormatter);
        rangeAxis.setTickUnit (new NumberTickUnit(50000));

        CombinedRangeCategoryPlot plot = new CombinedRangeCategoryPlot(rangeAxis);
        plot.setRangePannable(true);

        plot.add(subplot4, 2);
        plot.add(subplot0, 1);
        //plot.add(subplot2, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        plot.setGap(-1);
        plot.setOutlineVisible(false);

        JFreeChart chart = new JFreeChart("",
                new Font("OpenSans", Font.BOLD, 12), plot, true);

        chart.getLegend().setFrame(BlockBorder.NONE);
        chart.getLegend().setPosition(RectangleEdge.BOTTOM);
        chart.getPlot().setBackgroundPaint(Color.white);

        // all bars
        plot.getRangeAxis().setMinorTickMarkOutsideLength(22F);
        plot.setFixedLegendItems(
                createLegendItems(
                        new String[]{
                                "Electric Bill","Solar Loan Payments","Opt. 30% Paydown at Mo.18","Net Savings \u00B9"
                        },
                        new Paint[]{
                                grey, orange, lightorange, blue
                        }));
        plot.setRangeGridlinePaint(Color.black);
        chart.setBackgroundPaint(Color.WHITE);
        plot.setBackgroundPaint(Color.GREEN);

        return chart;
    }

    private LegendItemCollection createLegendItems(String[] labels, Paint[] colors) {
        LegendItemCollection result = new LegendItemCollection();
        for(int i=0; i < labels.length; i++){
            LegendItem item = new LegendItem(labels[i],labels[i],labels[i],labels[i], new Rectangle(20, 20), colors[i]);
            result.add(item);
        }
        return result;
    }

}
